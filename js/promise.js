function agreement(resolve, reject) {
  const sum = 1 + 2;
  if (sum == 2) {
    resolve("1+1 = 2");
  } else {
    reject("1+2 != 2");
  }
}

const sumPromise = new Promise(agreement);

sumPromise
  .then((success) => {
    console.log("promise resolved in success", success);
  })
  .catch((err) => {
    console.log("promise resolved in fail", err);
  });

//timeout
var buyToy = false;
const birthdayToyPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    buyToy = false;
    if (buyToy) {
      resolve("new toy bought for birthday");
    } else {
      reject("didn't buy new toy for birhtday");
    }
  }, 5000);
});

birthdayToyPromise
  .then((success) => console.log("Enjoy the gift"))
  .catch((success) => console.log("Cry for the gift"));
