var loggedInUser = sessionStorage.getItem("loggedInUser");
if (!loggedInUser) {
  location.href = "../login.html";
}

var logoutBtn = document.getElementById("logout-btn");
logoutBtn.addEventListener("click", function (e) {
  sessionStorage.removeItem("loggedInUser");
  location.href = "../index.html";
});

/*Todo code */
var todoSerialNumber = 0;
//call to the server to get the list of todos

const todoPromise = fetch("http://localhost:3000/todos");
todoPromise
  .then((response) => {
    console.log("got the success response", response);
    const dataPromise = response.json();
    dataPromise
      .then((todo) => {
        console.log("todo:", todo);
        let todoTable = document.getElementById("todo-list");
        for (let i = 0; i < todo.length; i++) {
          let row = document.createElement("tr");
          row.innerHTML = `
                            <td>${++todoSerialNumber}</td>
                            <td>
                              <span>${todo[i].description}</span>
                              <input type="text" class="edit-input" value="${
                                todo[i].description
                              }"> </input>
                              <div class="todo-id">${todo[i]._id}</div>
                            </td>
                            <td>
                              <button class="btn edit-btn" onclick="editTodo(this)">Edit</button>
                              <button class="btn save-btn" onclick="saveTodo(this)">Save</button>
                              <button class="btn" onclick="deleteTodo(this)">Delete</button>
                            </td>
                    `;
          todoTable.appendChild(row);
        }
      })
      .catch((err) => console.log("err in parsing", err));
  })
  .catch((err) => {
    console.log("got the error responose", err);
  });

//----------

var addBtn = document.getElementById("add-button");

addBtn.addEventListener("click", function (e) {
  e.preventDefault();

  let inputElement = document.getElementById("todo-input");
  let todoDescription = inputElement.value;
  console.log("todo:", todoDescription);
  if (todoDescription) {
    // call api -> save todo in mongo
    const data = {
      description: todoDescription,
    };
    const todoPromise = fetch("http://localhost:3000/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    todoPromise
      .then((res) => {
        const dataPromise = res.json();
        dataPromise.then((data) => {
          console.log("data", data);
          inputElement.className = "";
          let row = document.createElement("tr");

          row.innerHTML = `
                              <td>${++todoSerialNumber}</td>
                              <td>
                                <span>${todoDescription}</span>
                                <input type="text" class="edit-input" value="${todoDescription}"> </input>
                                <div class="todo-id">${data.todoID}</div>
                              </td>
                              <td>
                                <button class="btn edit-btn" onclick="editTodo(this)">Edit</button>
                                <button class="btn save-btn" onclick="saveTodo(this)">Save</button>
                                <button class="btn" onclick="deleteTodo(this)">Delete</button>
                              </td>
                      `;

          let todoTable = document.getElementById("todo-list");
          todoTable.appendChild(row);
          inputElement.value = "";
        });
      })
      .catch((err) => {
        console.log("Error", err);
        alert("could not add todo!");
      });
  } else {
    let inputElement = document.getElementById("todo-input");
    inputElement.className = "error";
  }
});

function deleteTodo(button) {
  console.log("deleted button", button);
  //api call -> delete -> todoID
  let row = button.parentNode.parentNode;
  const todoIDContainer = row.querySelector(".todo-id");
  const todoID = todoIDContainer.innerHTML;
  const deleteURL = `http://localhost:3000/todos/${todoID}`;
  const deletePromise = fetch(deleteURL, {
    method: "DELETE",
  });
  deletePromise
    .then(() => {
      console.log("row", row);
      //remove it from table
      let todoTable = document.getElementById("todo-list");
      todoTable.removeChild(row);
    })
    .catch((e) => {
      console.log("delete errror", e);
      alert("Failed to delete todo");
    });
}

function editTodo(button) {
  console.log("edit button clicked", button);
  //hide span
  //find span
  let row = button.parentNode.parentNode;
  let span = row.querySelector("span");
  //hide
  span.style.display = "none";
  //show input
  ///find
  let input = row.querySelector(".edit-input");
  //show
  input.style.display = "inline";

  //hide edit button
  let editButton = row.querySelector(".edit-btn");
  editButton.style.display = "none";
  //show save button
  let saveButton = row.querySelector(".save-btn");
  saveButton.style.display = "inline";
}

function saveTodo(button) {
  console.log("save todo");
  let row = button.parentNode.parentNode;
  let input = row.querySelector(".edit-input");
  let newDescription = input.value;
  //hide input
  input.style.display = "none";

  //show text
  let span = row.querySelector("span");
  span.innerHTML = newDescription;
  span.style.display = "inline";

  //hide save button
  let saveButton = row.querySelector(".save-btn");
  saveButton.style.display = "none";

  //show edit button
  let editButton = row.querySelector(".edit-btn");
  editButton.style.display = "inline";
}

//Timeout

setTimeout(function () {
  console.log("Lets get started");
}, 5000);
