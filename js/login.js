let loginForm = document.getElementById("login-form");
loginForm.addEventListener("submit", function (e) {
  e.preventDefault();
  console.log("form submited", e);
  //validation code
  let enteredUserName = document.getElementById("user-name").value;
  let enteredPassword = document.getElementById("password").value;
  console.log("enteredUserName", enteredUserName);
  console.log("enteredPassword", enteredPassword);
  if (enteredUserName && enteredPassword) {
    sessionStorage.setItem("loggedInUser", enteredUserName);
    location.href = "../todo.html";
  } else {
    console.log("missing field values");
    let errMessage = "";
    if (!enteredUserName) {
      errMessage = "User name";
      document.getElementById("user-name").className = "input-error";
    } else {
      document.getElementById("user-name").className = "input-success";
    }
    if (!enteredPassword) {
      if (errMessage) {
        errMessage += ", ";
      }
      errMessage += " Password ";
      document.getElementById("password").className = "input-error";
    } else {
      document.getElementById("password").className = "input-success";
    }
    document.getElementById("error-container").innerHTML =
      errMessage + " is required";
  }
});
